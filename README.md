# YetAnotherForth

> Университет ИТМО,  
> кафедра Вычислительной техники,  
> 2 курс, 2018  
> Системное программное обеспечение  
> Преподаватель - Игорь Жирков ([GitHub](https://github.com/sayon))  

---
### Быстрый старт
- Для сборки проекта достаточно клонировать репозиторий и выполнить ```make```
- Для запуска интерпретатора сразу после сборки выполните ```make run```

### Реализованные слова
##### Структура и содержание модулей
- ***dict.inc*** - Инициализирующий файл, подключает все остальные модули по необходимости
- ***aritchmetic.inc*** - ``` +, -, *, /, %, < ```
- ***logic_bitwise.inc*** - ``` and, or, not, lor, land, = ```
- ***data_stack.inc*** - ``` drop, swap, dup, rot, buf, ., .S, !, @, c@, c!, pushmode_addr, isimmediate, initcmd ```
- ***return_stack.inc*** - ``` >r, r>, r@ ```
- ***wrappers.inc*** - ``` read, prints, printnl, parsei, compare, key, emit, cfa, find ```
- ***colon.inc*** - ``` ', double, >, :, ; ```
- ***system.inc*** - ``` isbranch, lit, pushlit, unsetbranch, saveword, savetnum, wasbranch, pushmode, buffer, comp_m, init_m, branch, branchifz, docol, ,, create, syscall, warn, bye, exit ```
